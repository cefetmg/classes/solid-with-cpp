#include <iostream>
#include <list>
#include <string>

using namespace std;

struct Shape {
    Shape() {}
    virtual void draw() = 0;
};

class ShapeDrawStrategy {

    public:
        virtual bool shouldDraw(Shape* shape) = 0;
};

class ShapeDrawerService {
    public:

        void drawAllShapes(list<Shape*> shapes) {
            for (auto shape : shapes) {
                shape->draw();
            }
        }

        void drawAllShapes(list<Shape*> shapes, ShapeDrawStrategy *strategy) {
            for (auto shape : shapes) {
                if (strategy->shouldDraw(shape)) {
                    shape->draw();
                }
            }
        }
};