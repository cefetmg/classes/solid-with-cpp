#include <iostream>
#include <string>
#include <list>

using namespace std;

struct Person {
    string name;
    string profession;
    int age;

    Person() {}

    Person(string name, string profession, int age): name(name), profession(profession), age(age) { }
};

class PersonWriter {

    private:
        FILE *ostream;

    public:

        PersonWriter(FILE *ostream): ostream(ostream) { }

        virtual void writePerson(Person) = 0;

        virtual void writeMultiplePersons(list<Person>) = 0;
};

class PersonReader {

    private:
        FILE *istream;

    public:

        PersonReader(FILE *istream): istream(istream) { }

        virtual Person readPerson() = 0;

        virtual list<Person>& readMultiplePersons() = 0;
};

class PersonReportService {

    public:

        virtual list<Person>& processPersons(list<Person>) = 0;
};