#include <iostream>
#include <string>

using namespace std;

struct Process {
    string address;
    int port;

    Process() {}

    Process(string address, int port): address(address), port(port) { }
};

struct Connection {
    Process target;

    Connection(Process target): target(target) { }
};

class ServiceHealthCheck {

    public:
        virtual int ping(Connection connection) = 0;
};

class DefaultConnectionService : public ServiceHealthCheck {

    public:
        virtual int sendMessage(Connection connection, string bytes) = 0;

        virtual int receiveMessage(Connection connection, int amountOfBytes, string& receivedBytes) = 0;
};

class SQLConnectionService : public ServiceHealthCheck {

    public:
        virtual int executeSQLStatement(Connection connection) = 0;
};