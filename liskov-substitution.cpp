#include <iostream>
#include <list>
#include <string>

using namespace std;

struct Person {
    string name;
    string address;
    int age;

    Person() {}

    Person(string name, string address, int age): name(name), address(address), age(age) { }

    string toString() {
        return name + "," + address + "," + to_string(age);
    }
};

struct Employee : public Person {
    string profession;

    Employee(): Person() { }

    Employee(string name, string address, int age, string profession): Person(name, address, age), profession(profession) { }

    string toString() {
        return Person::toString() + "," + profession;
    }

};

class PersonWriter {

    private:
        FILE *ostream;

    PersonWriter(FILE *ostream): ostream(ostream) { }

    public:

        virtual void writePerson(Person) = 0;

        virtual void writeMultiplePersons(list<Person>) = 0;
};

class PersonReader {

    private:
        FILE *istream;

    PersonReader(FILE *istream): istream(istream) { }

    public:

        virtual Person readPerson() = 0;

        virtual list<Person>& readMultiplePersons() = 0;
};

class PersonReportService {

    public:

        virtual list<Person>& processPersons(list<Person>) = 0;
};